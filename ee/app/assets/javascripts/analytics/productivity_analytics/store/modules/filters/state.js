export default () => ({
  groupNamespace: null,
  projectPath: null,
  filters: '',
  startDate: null,
  endDate: null,
});
